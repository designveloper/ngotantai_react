import React, { Component } from "react";
import "./App.css";
import { FormControl, Button } from "react-bootstrap";
export class Popup extends Component {
  createTasks(item) {
    return (
      <li>
        {" "}
        <input type="checkbox" id="btn-checkbox" />{" "}
        <li className="newTask" key={item.key}>
          {" "}
          {item.text}{" "}
        </li>{" "}
        <button
          className="btn"
          onClick={() => this.props.handleDelete(item.key)}
        >
          {" "}
          <i className="fa fa-close" />{" "}
        </button>{" "}
      </li>
    );
  }
  render() {
    var todoEntries = this.props.entries;
    var listItems = todoEntries.map(item => this.createTasks(item));
    return (
      <section>
        {" "}
        <ul className="theList"> {listItems}</ul>{" "}
      </section>
    );
  }
}

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: JSON.parse(localStorage.getItem("items")) || [],
      countTask: 0
    };
    this.addItem = this.addItem.bind(this);
    this.onlik = this.onlik.bind(this);
  }
  addItem() {
    if (this.refs.inputElement.value !== "") {
      var newItem = {
        text: this.refs.inputElement.value,
        key: Date.now()
      };
      this.setState(prevState => {
        return {
          items: prevState.items.concat(newItem),
          countTask: prevState.countTask + 1
        };
      });
      this.refs.inputElement.value = "";
    }
    localStorage.setItem("items", JSON.stringify(this.state.items));
  }
  onlik(e) {
    e.preventDefault();
    this.addItem();
  }
  handleDelete = itemKey => {
    const newIems = this.state.items.filter(item => item.key !== itemKey);
    this.setState({
      items: newIems
    });
    this.setState(prevState => {
      return {
        countTask: prevState.countTask - 1
      };
    });
  };
  render() {
    return (
      <div className="App">
        {" "}
        <header className="App-header">
          {" "}
          <h1 className="App-title"> MY TASK </h1>{" "}
          <form onSubmit={() => this.addItem}>
            {" "}
            {/* <i class="arrow down" /> */}{" "}
            <input
              type="text"
              ref="inputElement"
              className="input-feild "
              placeholder="Field your task here to do"
            />{" "}
            <button onClick={this.onlik} className="btn-submit" type="submit">
              {" "}
              Submit{" "}
            </button>{" "}
          </form>{" "}
          <Popup
            entries={this.state.items}
            handleDelete={this.handleDelete}
          />{" "}
          <ul className="footer">
            {" "}
            <li>
              {" "}
              {this.state.countTask}
              : left{" "}
            </li>{" "}
            <ul className="sub-footer">
              {" "}
              <li className="active"> all </li> <li> active </li>{" "}
              <li> complete </li>{" "}
            </ul>{" "}
          </ul>{" "}
        </header>{" "}
        <footer className="howitworks">
          {" "}
          <footer className="info">
            {" "}
            <p> Double - click to edit a todo </p> <p> Cover </p>{" "}
          </footer>{" "}
        </footer>{" "}
      </div>
    );
  }
}

export default TodoList;
